/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author informatics
 */

import java.util.Scanner;

public class Lab1 {
    static char[][] table = { { '-', '-', '-' },
                            { '-', '-', '-' },
                            { '-', '-', '-' } };
    static int row, col;
    static char player = 'X';
    static int countTurn;
public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(); 
        }
    }
public static void inputRowCol(){
    Scanner sc = new Scanner(System.in);
    System.out.println("Turn"+" "+player);
    System.out.print("Input row col:"); 
    row = sc.nextInt();
    col = sc.nextInt();
    if (checkAlready(row, col)) {
            System.out.println("This point has already been chosen by the player");
            showTable();
            System.out.println("Please input RowCol again");
            inputRowCol();
        }
    table[row][col] = player;
    
}
static boolean checkAlready(int Row, int Col) {
        if (table[Row][Col] == 'X' || table[Row][Col] == 'O')
            return true;
        return false;
    }
public static void switchPlayer() {
        if (player == 'X')
            player = 'O';
        else
            player = 'X';
    }
    static boolean checkWinRow() {
        for (int c = 0; c < 3; c++) {
            if (table[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWinCol() {
        for (int r = 0; r < 3; r++) {
            if (table[r][col] != player) {
                return false;
            }
        }
        return true;
    }
    static boolean checkWin() {
        if (checkWinCol() || checkWinRow() || checkCross1() || checkCross2())
            return true;
        return false;
    }
    static boolean checkCross1() {
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player)
            return true;
        return false;
    }

    static boolean checkCross2() {
        if (table[0][2] == player && table[1][1] == player && table[2][0] == player)
            return true;
        return false;
    }
    public static void main(String[] args) {
        System.out.println("Welcome to XO RowCol Games!");
        for (countTurn = 1; countTurn < 10; countTurn++){
           showTable();
           inputRowCol();
           if(checkWin()){
               showTable();
               System.out.println("Player:"+" "+player+" "+"Win!!!");
               break;
           }
           switchPlayer();
        }
        if (countTurn == 10) {
            showTable();
            System.out.println("Player Draw!");
        }
    }
}
